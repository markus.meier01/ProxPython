# -*- coding: utf-8 -*-
"""
The "Algorithms"-module contains all algorithms provided by the ProxToolbox.
"""

#from algorithms import Algorithm
from .AP import *
from .HPR import *
# from .PALM import *
from .RAAR import *

__all__ = ["AP","HPR","RAAR"]
