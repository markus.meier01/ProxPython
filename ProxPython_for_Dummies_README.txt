So, you made it this far.  Congratulations. There are three paths to 
enlightenment:  the Way of Faith, the Way of Action and the Way 
of Contemplation.  This README follows the Way of Action to gain enlightenment
in python and proxtoolbox.  You've made the first step by just 
getting to the question.  

1. You need to get your confidence up by running a demo in python.  
So the first thing to do is open another X-terminal window and 
change directories to the same directory 
you are reading this from.  At the shell-prompt, type 

python3

This will open an instance of python3.##.  If your ipython interpreter
calls up version 3 of python, then you can type 

ipython

If you don't understand that last statement, skip it, it's not important.
Either way, if you see a different cursor prompt, then your in.  Congratulations, 
you have graduated to step 2!

2. Type 

import numpy as np

at the python prompt.   Don't ask why, just do it - it is not interesting.
But you should always do it without fail every time you open python for numerical 
mathematics.  It's like washing your hands before you eat, except the opposite, 
if that makes any sense, which it probably doesn't, so just do it and stop asking 
questions.  There will be a lot of these kind of command you will be told to type 
without explanation, so we'll just denote them JDI for ``just do it".  

3. Demo-specific JDI:  enter 

from proxtoolbox.Problems import Sudoku

at the python prompt.   

4. Demo-specific JDI: enter

ps=Sudoku()

at the python prompt.

5. Demo-specific JDI: type

ps.solve()

at the python prompt.

6. Demo-specific JDI: type
   
ps.show()

at the python prompt.

7. Did a graphics window appear with two Sudoku boards (one unsolved, one solved) and two graphs
underneath?  Congratulations, you've just taken your first step into a larger world!
 

Got that?  Great.  Let's try it again, with another demo.  First, to keep python running fast
lets clear out the current session by typing

exit()

at the python prompt.  Yup.  You're back in the X-window shell.  Repeat steps 1-2 above. 

3'.  Demo-specific JDI:  enter

from proxtoolbox.Problems import Ptychography

at the python prompt.

4'. Demo-specific JDI: enter

pp=Ptychography()

at the python prompt.

5'. Demo-specific JDI: enter

pp.solve()

at the python prompt.

6'. Demo-specific JDI: enter

pp.show()

at the python prompt.

7'. Did several graphics windows appear with lots of graphs and pictures? 
Congratulations, you've just taken your second step into a larger world! 
Ptyriffic isn't it? Ok, let's take a look at one final example... But first
don't forget to type

exit()

1''. For this example, we already done the hard work for you. This time we'll
run the ProxToolbox from using a "DRprotein_demo.py" script inside the TestSuite
subdirectory. You can peek inside it
by openning it by your favourite editor, or even simpler type

cat DRprotein_demo.py

from the prompt in the TestSuite directory.  
See how its got all the algorithmic parameters already in the file. Pretty neat huh?

2''. Ok. Time to run the script! It going to take a bit longer... actually a lot longer
than the first two examples (hours maybe). We can work on speeding it up later. Type

python3 DRprotein_demo.py

(again, from in the TestSuite subdirectory). 
You should see some (boring) information about the problem instance, etc. While the algorithm
is running you a report of its progress even 10 iterations, and will stop when the relative
error drops below 10e-5. 

If you waited long enough "Jmol", a molecular viewer written in Java (which you should already
have installed) will open. Hopefully it looks like a molecule.

   
 
